# Gist Challenge

Gist Challenge é um aplicativo de Teste utilizado para o Recrutamento
da Luizalabs.

### Prerequisites

Para baixar e executar este projeto é necessário ter instalado
previamente estas ferramentas:

- [x] Git
- [x] Android Studio 3.5+
- [x] Android SDK
- [x] JDK 1.8+

## Getting Started

Estas instruções vão possibilitar você a ter uma cópia e executar este 
projeto na sua máquina local para fins de desenvolvimento e testes.

Primeiramente, você precisa clonar este projeto num diretório à sua 
escolha utilizando o comando abaixo no seu terminal:

``` 
git clone https://gitlab.com/luis_ccm/gist-challenge.git
```

### Instalando

Para prosseguir com a instalação do projeto, é necessário que já tenha
previamente instalado todas as ferramentas pré-requisitos.

Abra o Android Studio e em seguida escolha a opção 
`Open an existing Android Studio Project` e selecione o diretório onde 
seu projeto foi clonado.

Após esse procedimento, o Android Studio irá configurar e baixar todas
as dependências necessárias automaticamente.

Caso seja necessário alguma instalação de algum SDK ou Tool específica
do Android SDK, um pop-up será exibido, basta aceitar e prosseguir.

## Running the tests

Para executar os testes e garantir que todo o projeto está corretamente
configurado, execute via terminal na pasta raiz do projeto o seguinte
comando:

```
run 'make test'
```

Todos os testes de unidade e instrumentados serão executados e deverão
ser finalizados com sucesso.


## Running

Para executar num device emulado ou real, basta selecionar o device
desejado e executar o seguinte comando:

```
CTRL + R
```

ou através desta sequência de cliques:

```
Run -> Run App
```
Ao realizar qualquer build no projeto, o mesmo executará o ktlint para 
analisar a formatação do código:
 ```
./gradlew ktlintCheck
```
 Caso não esteja em conformidade das diretrizes estabelecidas o build falhará, 
 evidenciando os arquivos impactados.
 
 Para formatar o código seguindo as diretrizes estabelecidas, executar o seguinte comando:
  
 ```
./gradlew ktlintFormat
```
## Arquitetura
 A arquitetura básica do app segue o padrão com Use Case e Repository Pattern, com separação da camada de Apresentação de
 dados (Activity, Fragment, ViewModel, etc..), com Domínio (Casos de Uso), e Dados (Repositório, Dados Locais, Dados Remotos, etc.. )
 

## Built With

* [Kotlin](https://kotlinlang.org/) Programming Language for Android
* [Koin](https://insert-koin.io/) - Dependency Injection
* [MockK](https://mockk.io/) - Mock Library
* [ktlint](https://ktlint.github.io/) - Code formatting for Kotlin
* [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client for Android
* [Android Jetpack](https://developer.android.com/jetpack/) - Jetpack is a suite of libraries to help developers follow best practices
* [Lottie](https://github.com/airbnb/lottie-android) - Render after effects animations
* [CircleImageView](https://github.com/hdodenhof/CircleImageView) - A circular ImageView for Android
* [Picasso](https://square.github.io/picasso) - A powerful image downloading and caching library for Android

## Authors

* **Luis Nascimento**
