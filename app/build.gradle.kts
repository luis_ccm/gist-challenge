plugins {
    id(AppConfiguration.Plugins.androidApplication)
    kotlin(AppConfiguration.Plugins.kotlinAndroid)
    kotlin(AppConfiguration.Plugins.kotlinAndroidExtensions)
    kotlin(AppConfiguration.Plugins.kapt)
    id(AppConfiguration.Plugins.kotlinKapt)
    id(AppConfiguration.Plugins.kotlinAndroidPlugin)
    id(AppConfiguration.Plugins.safeArgs)
}

android {
    compileSdkVersion(AppConfiguration.compileSdkVersion)
    defaultConfig {
        applicationId = AppConfiguration.applicationId
        minSdkVersion(AppConfiguration.minSdkVersion)
        targetSdkVersion(AppConfiguration.targetSdkVersion)
        versionCode = AppConfiguration.versionCode
        versionName = AppConfiguration.versionName

        vectorDrawables.useSupportLibrary = true

        testInstrumentationRunner = AppConfiguration.testInstrumentationRunner
    }
    buildTypes {

        getByName(AppConfiguration.BuildType.Release.name) {
            isMinifyEnabled = AppConfiguration.BuildType.Release.isMinifyEnabled
            proguardFiles(
                getDefaultProguardFile(AppConfiguration.BuildType.Release.defaultProguardFile),
                AppConfiguration.BuildType.Release.defaultProguardRules
            )
        }
        getByName(AppConfiguration.BuildType.Debug.name) {
            isMinifyEnabled = AppConfiguration.BuildType.Debug.isMinifyEnabled
            AppConfiguration.BuildType.Debug.suffix.run {
                applicationIdSuffix = this
                versionNameSuffix = this
            }
        }
    }

    flavorDimensions(AppConfiguration.Dimension.Type.name)

    productFlavors {
        create(AppConfiguration.ProductFlavor.Netshoes.name) {
            setDimension(AppConfiguration.ProductFlavor.Netshoes.dimension.name)
            AppConfiguration.ProductFlavor.Netshoes.name.run {
                applicationIdSuffix = this
                versionNameSuffix = this
            }
        }
        create(AppConfiguration.ProductFlavor.MagazineLuiza.name) {
            setDimension(AppConfiguration.ProductFlavor.MagazineLuiza.dimension.name)
            AppConfiguration.ProductFlavor.MagazineLuiza.name.run {
                applicationIdSuffix = this
                versionNameSuffix = this
            }
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    kotlinOptions.apply {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    testOptions {
        unitTests.isReturnDefaultValues = true
    }
}

val ktlint by configurations.creating


dependencies {
    AppConfiguration.libsImplementations.forEach { lib ->
        implementation(lib)
    }
    ktlint(Libs.ktlint)
    kapt(Libs.room_ktx)
    AppConfiguration.testImplementationLibs.forEach { lib ->
        testImplementation(lib)
    }

    AppConfiguration.androidTestImplementationLibs.forEach { lib ->
        androidTestImplementation(lib)
    }
}

val outputDir = "${project.buildDir}/reports/ktlint/"
val inputFiles = project.fileTree(mapOf("dir" to "src", "include" to "**/*.kt"))

val ktlintCheck by tasks.creating(JavaExec::class) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    group = AppConfiguration.Lint.Task.Check.group
    description = AppConfiguration.Lint.Task.Check.description
    classpath = ktlint
    main = AppConfiguration.Lint.Task.Check.main
    args = AppConfiguration.Lint.Task.Check.args
}

val ktlintFormat by tasks.creating(JavaExec::class) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    group = AppConfiguration.Lint.Task.Format.group
    description = AppConfiguration.Lint.Task.Format.description
    classpath = ktlint
    main = AppConfiguration.Lint.Task.Format.main
    args = AppConfiguration.Lint.Task.Format.args
}

getTasksByName(AppConfiguration.Task.preBuild, true).first().dependsOn(ktlintCheck)