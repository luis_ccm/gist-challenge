package br.com.luizalabs.gistchallenge.application

import android.app.Application
import br.com.luizalabs.gistchallenge.di.FeatureModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            loadKoinModules(FeatureModules.modules)
        }
    }
}
