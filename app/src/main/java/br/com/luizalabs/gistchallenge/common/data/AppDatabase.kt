package br.com.luizalabs.gistchallenge.common.data

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.local.GistFavoriteDao
import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model.GistFavoriteModel

@Database(
    entities = [GistFavoriteModel::class],
    version = AppDatabase.databaseVersion,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val databaseVersion = 1
    }

    abstract fun GistFavoriteDao(): GistFavoriteDao
}
