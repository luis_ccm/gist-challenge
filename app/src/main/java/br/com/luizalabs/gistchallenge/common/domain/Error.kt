package br.com.luizalabs.gistchallenge.common.domain

import java.lang.Exception

sealed class Error(val exception: Exception) {
    open class BusinessError(exception: Exception) : Error(exception)
    open class Network(exception: Exception) : Error(exception)
}
