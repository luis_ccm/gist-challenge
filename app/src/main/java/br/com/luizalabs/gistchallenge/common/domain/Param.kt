package br.com.luizalabs.gistchallenge.common.domain

data class Param<Input>(val input: Input)
