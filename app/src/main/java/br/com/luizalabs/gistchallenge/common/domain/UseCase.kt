package br.com.luizalabs.gistchallenge.common.domain

abstract class UseCase<Input, out Output> {
    abstract suspend fun execute(vararg param: Param<Input>): State<Output>

    fun mapToStateFailure(exception: Exception): State.Failure {
        return State.Failure(Error.BusinessError(exception))
    }
}
