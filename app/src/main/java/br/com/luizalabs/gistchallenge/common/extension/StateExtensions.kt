package br.com.luizalabs.gistchallenge.common.extension

import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.ui.ViewState

fun <T> State<T>.toViewState(): ViewState<T> {
    return when (this) {
        is State.Success -> ViewState.Success(data)
        is State.Failure -> ViewState.Failure(error)
    }
}
