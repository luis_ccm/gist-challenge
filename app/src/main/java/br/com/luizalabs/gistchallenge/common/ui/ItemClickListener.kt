package br.com.luizalabs.gistchallenge.common.ui

interface ItemClickListener<T> {
    fun onItemClick(model: T)
}
