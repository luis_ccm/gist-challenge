package br.com.luizalabs.gistchallenge.common.ui

interface ItemClickListenerPosition<T> {
    fun onItemClick(model: T, position: Int)
}
