package br.com.luizalabs.gistchallenge.common.ui

import br.com.luizalabs.gistchallenge.common.domain.Error

sealed class ViewState<out T> {
    data class Loading(val fistTime: Boolean = true) : ViewState<Nothing>()
    data class Success<T>(val data: T) : ViewState<T>()
    data class Failure(val error: Error, val fistTime: Boolean = true) : ViewState<Nothing>()
    object Done : ViewState<Nothing>()
}
