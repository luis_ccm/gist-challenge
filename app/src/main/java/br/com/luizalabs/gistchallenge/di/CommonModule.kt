package br.com.luizalabs.gistchallenge.di

import androidx.room.Room
import br.com.luizalabs.gistchallenge.common.data.AppDatabase
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object CommonModule {
    private const val url = "https://api.github.com/gists/"
    private const val dataBaseName = "gists-database"

    val databaseModule = module {
        single {
            return@single Room.databaseBuilder(androidApplication(), AppDatabase::class.java, dataBaseName)
                .build()
        }
    }
    val remoteModule = module {
        single {
            OkHttpClient.Builder().build()
        }
        single {
            GsonBuilder().create()
        }
        single {
            Retrofit.Builder()
                .baseUrl(url)
                .client(get())
                .addConverterFactory(GsonConverterFactory.create(get()))
                .build()
        }
    }
}
