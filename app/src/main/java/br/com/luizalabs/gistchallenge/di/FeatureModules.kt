package br.com.luizalabs.gistchallenge.di

import br.com.luizalabs.gistchallenge.feature.favorites.di.FavoriteModule
import br.com.luizalabs.gistchallenge.feature.home.di.HomeModule

object FeatureModules {
    val modules = listOf(CommonModule.remoteModule, HomeModule.graph, CommonModule.databaseModule, FavoriteModule.graph)
}
