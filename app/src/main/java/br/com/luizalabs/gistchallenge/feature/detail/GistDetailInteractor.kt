package br.com.luizalabs.gistchallenge.feature.detail

import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel

interface GistDetailInteractor {
    interface View {
        fun showData(model: GistResponseModel)
    }
}
