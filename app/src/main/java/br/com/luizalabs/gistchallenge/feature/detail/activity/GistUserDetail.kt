package br.com.luizalabs.gistchallenge.feature.detail.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs
import br.com.luizalabs.gistchallenge.R
import br.com.luizalabs.gistchallenge.feature.detail.GistDetailInteractor
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_gist_user_detail.*

class GistUserDetail : AppCompatActivity(), GistDetailInteractor.View {

    private val args: GistUserDetailArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gist_user_detail)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        showData(args.param)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    override fun showData(model: GistResponseModel) {
        Picasso.get()
            .load(model.owner.avatar)
            .error(R.drawable.ic_placeholder)
            .into(gistDetailPhoto)

        gistDetailName.text = model.owner.name
        gistDetailItemType.text = model.type
        gistDetailDate.text = getString(R.string.title_created_at, model.createdAt)
        gistDetailDescription.text = model.description
        gistDetailUrl.text = getString(R.string.title_url, model.owner.url)
    }

    override fun onStop() {
        super.onStop()
        Picasso.get().cancelRequest(gistDetailPhoto)
    }
}
