package br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model.GistFavoriteModel

@Dao
interface GistFavoriteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(user: GistFavoriteModel)

    @Query("SELECT * FROM GistFavoriteModel")
    suspend fun getFavoritesGists(): List<GistFavoriteModel>

    @Query("DELETE FROM GistFavoriteModel WHERE id =:id")
    suspend fun deleteFavorite(id: String)
}
