package br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model

import androidx.room.Entity

@Entity(primaryKeys = ["id"])
data class GistFavoriteModel(val id: String, val name: String, val avatar: String)
