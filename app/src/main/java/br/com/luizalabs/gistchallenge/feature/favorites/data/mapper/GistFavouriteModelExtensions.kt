package br.com.luizalabs.gistchallenge.feature.favorites.data.mapper

import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model.GistFavoriteModel
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite

fun GistFavoriteModel.mapToDomain() = GistFavorite(id = this.id, name = this.name, avatar = this.avatar)
