package br.com.luizalabs.gistchallenge.feature.favorites.data.repository

import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.local.GistFavoriteDao
import br.com.luizalabs.gistchallenge.feature.favorites.data.mapper.mapToDomain
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository
import br.com.luizalabs.gistchallenge.feature.home.domain.mapper.mapToData
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel

class GistsFavoriteRepositoryImpl(private val dao: GistFavoriteDao) : GistsFavoritesRepository {
    override suspend fun insertFavorite(responseModel: GistResponseModel) =
        dao.insert(responseModel.mapToData())

    override suspend fun getFavorites(): List<GistFavorite> {
        return dao.getFavoritesGists().map {
            it.mapToDomain()
        }
    }

    override suspend fun removeFavorite(id: String) = dao.deleteFavorite(id)
}
