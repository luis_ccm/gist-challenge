package br.com.luizalabs.gistchallenge.feature.favorites.di

import br.com.luizalabs.gistchallenge.feature.favorites.domain.GetFavoritesUseCaseImp
import br.com.luizalabs.gistchallenge.feature.favorites.domain.RemoveFavoriteUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.favorites.ui.viewModel.GistFavoriteViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object FavoriteModule {
    val graph = module {
        factory {
            GetFavoritesUseCaseImp(repository = get())
        }

        factory {
            RemoveFavoriteUseCaseImpl(
                repository = get()
            )
        }
        viewModel {
            GistFavoriteViewModel(useCase = get(), removeFavoriteUseCase = get())
        }
    }
}
