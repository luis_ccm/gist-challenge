package br.com.luizalabs.gistchallenge.feature.favorites.domain

import br.com.luizalabs.gistchallenge.common.domain.Error
import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.domain.UseCase
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository

typealias GetFavoritesUseCase = UseCase<Nothing, List<GistFavorite>>

class GetFavoritesUseCaseImp(private val repository: GistsFavoritesRepository) :
    GetFavoritesUseCase() {
    override suspend fun execute(vararg param: Param<Nothing>): State<List<GistFavorite>> {
        return try {
            val result = repository.getFavorites()
            if (result.isNotEmpty()) State.Success(repository.getFavorites()) else State.Failure(
                Error.BusinessError(IllegalStateException())
            )
        } catch (exception: Exception) {
            mapToStateFailure(exception)
        }
    }
}
