package br.com.luizalabs.gistchallenge.feature.favorites.domain

import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.domain.UseCase
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository

typealias RemoveFavoriteUseCase = UseCase<String, Unit>

class RemoveFavoriteUseCaseImpl(private val repository: GistsFavoritesRepository) : RemoveFavoriteUseCase() {
    override suspend fun execute(vararg param: Param<String>): State<Unit> {
        return try {
            State.Success(repository.removeFavorite(param.first().input))
        } catch (exception: Exception) {
            mapToStateFailure(exception)
        }
    }
}
