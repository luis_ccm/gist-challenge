package br.com.luizalabs.gistchallenge.feature.favorites.domain.model

data class GistFavorite(val id: String, val name: String, val avatar: String)
