package br.com.luizalabs.gistchallenge.feature.favorites.domain.repository

import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel

interface GistsFavoritesRepository {
    suspend fun insertFavorite(responseModel: GistResponseModel)
    suspend fun getFavorites(): List<GistFavorite>
    suspend fun removeFavorite(id: String)
}
