package br.com.luizalabs.gistchallenge.feature.favorites.ui

import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite

interface FavoriteInteractor {
    interface View {
        fun observeResults()
        fun showLoading()
        fun showError()
        fun showData(result: List<GistFavorite>)
        fun configureView()
        fun removeFavorite(
            item: GistFavorite,
            position: Int
        )
    }

    interface ViewModel {
        fun getFavorites()
        fun removeFavorite(item: GistFavorite)
    }
}
