package br.com.luizalabs.gistchallenge.feature.favorites.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.luizalabs.gistchallenge.R
import br.com.luizalabs.gistchallenge.common.extension.observe
import br.com.luizalabs.gistchallenge.common.ui.ItemClickListenerPosition
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import br.com.luizalabs.gistchallenge.feature.favorites.ui.FavoriteInteractor
import br.com.luizalabs.gistchallenge.feature.favorites.ui.fragment.adapter.FavoritesAdapter
import br.com.luizalabs.gistchallenge.feature.favorites.ui.viewModel.GistFavoriteViewModel
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavoritesFragment : Fragment(), FavoriteInteractor.View {

    private val viewModel: GistFavoriteViewModel by viewModel()

    private val adapter: FavoritesAdapter by lazy {
        FavoritesAdapter(
            clickListener = object : ItemClickListenerPosition<GistFavorite> {
                override fun onItemClick(model: GistFavorite, position: Int) {
                    removeFavorite(model, position)
                }
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeResults()
        configureView()
        viewModel.getFavorites()
    }

    override fun observeResults() {
        observe(viewModel.getFavoritesLiveData()) { data ->
            when (data) {
                is ViewState.Loading -> showLoading()
                is ViewState.Success -> showData(data.data)
                is ViewState.Failure -> showError()
                else -> {
                }
            }
        }

        observe(viewModel.getRemoveLiveData()) { data ->
            when (data) {
                is ViewState.Done -> viewModel.getFavorites()
            }
        }
    }

    override fun configureView() {
        gistFavoriteList.adapter = adapter
    }

    override fun showLoading() {
        gistFavoriteLoading.visibility = View.VISIBLE
        gistFavoriteError.visibility = View.GONE
        gistFavoriteList.visibility = View.GONE
    }

    override fun showError() {
        gistFavoriteLoading.visibility = View.GONE
        gistFavoriteError.visibility = View.VISIBLE
        gistFavoriteList.visibility = View.GONE
    }

    override fun showData(result: List<GistFavorite>) {
        gistFavoriteLoading.visibility = View.GONE
        gistFavoriteError.visibility = View.GONE
        gistFavoriteList.visibility = View.VISIBLE
        adapter.submitList(result)
    }

    override fun removeFavorite(
        item: GistFavorite,
        position: Int
    ) {
        adapter.removeItem(position)
        viewModel.removeFavorite(item)
    }
}
