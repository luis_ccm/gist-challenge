package br.com.luizalabs.gistchallenge.feature.favorites.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.RecyclerView
import br.com.luizalabs.gistchallenge.R
import br.com.luizalabs.gistchallenge.common.ui.ItemClickListenerPosition
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.favorite_gist_list_item.view.*

class FavoritesAdapter(private var items: MutableList<GistFavorite> = mutableListOf(), private val clickListener: ItemClickListenerPosition<GistFavorite>) :
    RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.favorite_gist_list_item, parent, false)
        return FavoritesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        holder.bind(items[position], clickListener)
    }

    class FavoritesViewHolder(item: View) : RecyclerView.ViewHolder(item), LifecycleObserver {
        fun bind(
            favorite: GistFavorite,
            clickListener: ItemClickListenerPosition<GistFavorite>
        ) {
            itemView.favoriteGistListItemName.text =
                itemView.context.getString(R.string.gist_list_name, favorite.name)
            Picasso.get()
                .load(favorite.avatar)
                .error(R.drawable.ic_placeholder)
                .into(itemView.favoriteGistListItemPhoto)
            itemView.fovoriteListItemButton.setOnClickListener {
                clickListener.onItemClick(favorite, adapterPosition)
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun cancelRequest() {
            Picasso.get().cancelRequest(itemView.favoriteGistListItemPhoto)
        }
    }

    fun submitList(items: List<GistFavorite>) {
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }
}
