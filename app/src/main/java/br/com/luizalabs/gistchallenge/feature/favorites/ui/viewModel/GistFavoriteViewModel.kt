package br.com.luizalabs.gistchallenge.feature.favorites.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.extension.toViewState
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.favorites.domain.GetFavoritesUseCaseImp
import br.com.luizalabs.gistchallenge.feature.favorites.domain.RemoveFavoriteUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import br.com.luizalabs.gistchallenge.feature.favorites.ui.FavoriteInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GistFavoriteViewModel(private val useCase: GetFavoritesUseCaseImp, private val removeFavoriteUseCase: RemoveFavoriteUseCaseImpl) :
    ViewModel(),
    FavoriteInteractor.ViewModel {

    private val favoritesLiveData = MutableLiveData<ViewState<List<GistFavorite>>>()
    private val removeFavoriteLiveData = MutableLiveData<ViewState<Unit>>()

    fun getFavoritesLiveData(): LiveData<ViewState<List<GistFavorite>>> = favoritesLiveData

    fun getRemoveLiveData(): LiveData<ViewState<Unit>> = removeFavoriteLiveData

    override fun getFavorites() {
        viewModelScope.launch {
            favoritesLiveData.value = ViewState.Loading()
            val result = withContext(Dispatchers.IO) {
                useCase.execute()
            }
            favoritesLiveData.value = result.toViewState()
            favoritesLiveData.value = ViewState.Done
        }
    }

    override fun removeFavorite(item: GistFavorite) {
        viewModelScope.launch {
            removeFavoriteLiveData.value = ViewState.Loading()
            val result = withContext(Dispatchers.IO) {
                removeFavoriteUseCase.execute(Param(item.id))
            }
            removeFavoriteLiveData.value = result.toViewState()
            removeFavoriteLiveData.value = ViewState.Done
        }
    }
}
