package br.com.luizalabs.gistchallenge.feature.home.data.datasource.network

import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistModelPayload

interface GistsRemote {
    suspend fun getPublicGists(maxItems: Int, page: Int): List<GistModelPayload>
}
