package br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.impl

import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.GistsRemote
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistModelPayload
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.service.GistsService

class GistsRemoteImpl(private val service: GistsService) : GistsRemote {
    override suspend fun getPublicGists(maxItems: Int, page: Int): List<GistModelPayload> =
        service.getPublicGists(maxItems, page)
}
