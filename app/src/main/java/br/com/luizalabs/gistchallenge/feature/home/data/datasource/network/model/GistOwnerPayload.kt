package br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model

import com.google.gson.annotations.SerializedName

data class GistOwnerPayload(
    @SerializedName("id")
    val id: Int,
    @SerializedName("avatar_url")
    val avatarUrl: String,
    @SerializedName("login")
    val login: String,
    @SerializedName("html_url")
    val url: String
)
