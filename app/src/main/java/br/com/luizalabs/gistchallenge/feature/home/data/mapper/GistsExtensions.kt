package br.com.luizalabs.gistchallenge.feature.home.data.mapper

import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistModelPayload
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistOwnerPayload
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistOwnerResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import java.text.SimpleDateFormat

private const val TypeKeyName = "type"

fun GistOwnerPayload.mapToDomain(): GistOwnerResponseModel =
    GistOwnerResponseModel(id = this.id, name = this.login, avatar = this.avatarUrl, url = this.url)

fun GistModelPayload.mapToDomain(): GistResponseModel {
    val typeString = try {
        val typeMap: Map<String, Any> = this.files.entries.firstOrNull()?.value as Map<String, Any>
        typeMap[TypeKeyName] as String?
    } catch (exception: Exception) {
        ""
    }

    val createdAt = try {
        SimpleDateFormat.getDateInstance().format(this.createdAt)
    } catch (exception: Exception) {
        ""
    }
    return GistResponseModel(
        id = this.id,
        description = this.description ?: "",
        type = typeString ?: "",
        createdAt = createdAt,
        owner = this.owner.mapToDomain()
    )
}
