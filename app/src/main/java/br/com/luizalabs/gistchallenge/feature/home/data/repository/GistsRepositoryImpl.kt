package br.com.luizalabs.gistchallenge.feature.home.data.repository

import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.GistsRemote
import br.com.luizalabs.gistchallenge.feature.home.data.mapper.mapToDomain
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistsRequestModel
import br.com.luizalabs.gistchallenge.feature.home.domain.repository.GistsRepository

class GistsRepositoryImpl(private val remote: GistsRemote) : GistsRepository {
    override suspend fun getPublicGists(model: GistsRequestModel): List<GistResponseModel> {
        val response = remote.getPublicGists(model.maxItems, model.page)
        return response.map { it.mapToDomain() }
    }
}
