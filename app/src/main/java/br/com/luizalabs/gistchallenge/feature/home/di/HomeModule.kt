package br.com.luizalabs.gistchallenge.feature.home.di

import br.com.luizalabs.gistchallenge.common.data.AppDatabase
import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.local.GistFavoriteDao
import br.com.luizalabs.gistchallenge.feature.favorites.data.repository.GistsFavoriteRepositoryImpl
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.GistsRemote
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.impl.GistsRemoteImpl
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.service.GistsService
import br.com.luizalabs.gistchallenge.feature.home.data.repository.GistsRepositoryImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.FilterGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.GetPublicGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.InsertGistFavoriteUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.repository.GistsRepository
import br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel.GistsOwnerFilterViewModel
import br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object HomeModule {
    val graph = module {
        single {
            provideGistsService(retrofit = get())
        }

        single<GistsRemote> {
            GistsRemoteImpl(service = get())
        }

        factory {
            provideGistFavoriteDao(appDatabase = get())
        }

        single<GistsRepository> {
            GistsRepositoryImpl(remote = get())
        }

        single<GistsFavoritesRepository> {
            GistsFavoriteRepositoryImpl(dao = get())
        }

        factory {
            GetPublicGistsUseCaseImpl(repository = get())
        }

        factory {
            InsertGistFavoriteUseCaseImpl(repository = get())
        }

        factory {
            FilterGistsUseCaseImpl()
        }

        viewModel {
            HomeViewModel(getPublicGistsUseCase = get(), insertGistFavoriteUseCase = get())
        }

        viewModel {
            GistsOwnerFilterViewModel(filterGistsUseCase = get())
        }
    }

    private fun provideGistsService(retrofit: Retrofit): GistsService =
        retrofit.create(GistsService::class.java)

    private fun provideGistFavoriteDao(appDatabase: AppDatabase): GistFavoriteDao =
        appDatabase.GistFavoriteDao()
}
