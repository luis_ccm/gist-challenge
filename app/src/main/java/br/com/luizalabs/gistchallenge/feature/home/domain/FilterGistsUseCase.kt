package br.com.luizalabs.gistchallenge.feature.home.domain

import br.com.luizalabs.gistchallenge.common.domain.Error
import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.domain.UseCase
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistSearchModel
import java.text.Normalizer

typealias FilterGistsUseCase = UseCase<GistSearchModel, List<GistResponseModel>>

class FilterGistsUseCaseImpl() : FilterGistsUseCase() {
    override suspend fun execute(vararg param: Param<GistSearchModel>): State<List<GistResponseModel>> {
        return try {
            val data = param.first().input
            val result = data.items.filter {
                val name = Normalizer.normalize(it.owner.name, Normalizer.Form.NFD)
                name.contains(Normalizer.normalize(data.query, Normalizer.Form.NFD))
            }
            if (result.isEmpty()) State.Failure(Error.BusinessError(IllegalStateException())) else State.Success(
                result
            )
        } catch (ex: Exception) {
            mapToStateFailure(ex)
        }
    }
}
