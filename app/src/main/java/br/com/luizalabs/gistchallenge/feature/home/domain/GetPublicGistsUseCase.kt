package br.com.luizalabs.gistchallenge.feature.home.domain

import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.domain.UseCase
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistsRequestModel
import br.com.luizalabs.gistchallenge.feature.home.domain.repository.GistsRepository

typealias GetPublicGistsUseCase = UseCase<GistsRequestModel, List<GistResponseModel>>

class GetPublicGistsUseCaseImpl(private val repository: GistsRepository) : GetPublicGistsUseCase() {

    override suspend fun execute(vararg param: Param<GistsRequestModel>): State<List<GistResponseModel>> {
        return try {
            State.Success(repository.getPublicGists(GistsRequestModel(param.first().input.maxItems, param.first().input.page)))
        } catch (exception: Exception) {
            mapToStateFailure(exception)
        }
    }
}
