package br.com.luizalabs.gistchallenge.feature.home.domain

import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.domain.UseCase
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import java.lang.Exception

typealias InsertGistFavoriteUseCase = UseCase<GistResponseModel, Unit>

class InsertGistFavoriteUseCaseImpl(private val repository: GistsFavoritesRepository) :
    InsertGistFavoriteUseCase() {
    override suspend fun execute(vararg param: Param<GistResponseModel>): State<Unit> {
        return try {
            State.Success(repository.insertFavorite(param.first().input))
        } catch (exception: Exception) {
            mapToStateFailure(exception)
        }
    }
}
