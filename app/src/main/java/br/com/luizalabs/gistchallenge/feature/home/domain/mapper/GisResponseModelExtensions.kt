package br.com.luizalabs.gistchallenge.feature.home.domain.mapper

import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model.GistFavoriteModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel

fun GistResponseModel.mapToData() =
    GistFavoriteModel(id = this.id, name = this.owner.name, avatar = this.owner.avatar)
