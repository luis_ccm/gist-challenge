package br.com.luizalabs.gistchallenge.feature.home.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GistOwnerResponseModel(
    val id: Int,
    val name: String,
    val avatar: String,
    val url: String
) : Parcelable {
    companion object {
        fun empty() = GistOwnerResponseModel(id = 0, name = "", avatar = "", url = "")
    }
}
