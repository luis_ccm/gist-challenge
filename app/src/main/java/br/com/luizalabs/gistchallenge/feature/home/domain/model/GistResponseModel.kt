package br.com.luizalabs.gistchallenge.feature.home.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GistResponseModel(
    val id: String,
    val type: String,
    val createdAt: String,
    val description: String,
    val owner: GistOwnerResponseModel
) : Parcelable {
    companion object {
        fun empty() = GistResponseModel(
            id = "",
            type = "",
            createdAt = "",
            description = "",
            owner = GistOwnerResponseModel.empty()
        )
    }
}
