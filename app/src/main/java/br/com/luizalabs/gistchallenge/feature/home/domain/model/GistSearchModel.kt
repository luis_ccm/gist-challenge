package br.com.luizalabs.gistchallenge.feature.home.domain.model

data class GistSearchModel(val query: String, val items: List<GistResponseModel>)
