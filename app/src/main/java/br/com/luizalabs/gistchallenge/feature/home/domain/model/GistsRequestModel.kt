package br.com.luizalabs.gistchallenge.feature.home.domain.model

data class GistsRequestModel(val maxItems: Int, val page: Int)
