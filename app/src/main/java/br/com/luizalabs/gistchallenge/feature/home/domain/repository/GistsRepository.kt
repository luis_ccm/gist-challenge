package br.com.luizalabs.gistchallenge.feature.home.domain.repository

import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistsRequestModel

interface GistsRepository {
    suspend fun getPublicGists(model: GistsRequestModel): List<GistResponseModel>
}
