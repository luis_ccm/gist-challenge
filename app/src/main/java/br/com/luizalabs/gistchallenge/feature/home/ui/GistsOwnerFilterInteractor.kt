package br.com.luizalabs.gistchallenge.feature.home.ui

import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel

interface GistsOwnerFilterInteractor {

    interface View {
        fun configureView()
        fun observeResults()
        fun showData(data: List<GistResponseModel>)
        fun showError()
    }

    interface ViewModel {
        fun filterList(query: String, items: List<GistResponseModel>)
    }
}
