package br.com.luizalabs.gistchallenge.feature.home.ui

import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel

interface PublicGistsInteractor {
    interface View {
        fun observeResults()
        fun initializeList()
        fun navigateToDetail(model: GistResponseModel)
        fun showError()
        fun showLoading()
        fun showData()
        fun insertFavorite(model: GistResponseModel)
        fun configureView()
        fun showBottomSheet()
    }

    interface ViewModel {
        fun insertFavorite(model: GistResponseModel)
    }
}
