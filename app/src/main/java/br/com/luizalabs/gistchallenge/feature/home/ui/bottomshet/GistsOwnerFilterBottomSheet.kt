package br.com.luizalabs.gistchallenge.feature.home.ui.bottomshet

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.luizalabs.gistchallenge.R
import br.com.luizalabs.gistchallenge.common.extension.observe
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.ui.GistsOwnerFilterInteractor
import br.com.luizalabs.gistchallenge.feature.home.ui.bottomshet.adapter.GistFilterAdapter
import br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel.GistsOwnerFilterViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.gist_owner_filter_bottom_sheet.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class GistsOwnerFilterBottomSheet(private val items: List<GistResponseModel>) :
    BottomSheetDialogFragment(),
    GistsOwnerFilterInteractor.View {

    private val viewModel: GistsOwnerFilterViewModel by viewModel()

    private val adapter: GistFilterAdapter by lazy {
        GistFilterAdapter(lifecycleOwner = lifecycle)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.gist_owner_filter_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView()
        observeResults()
    }

    override fun configureView() {
        dialog?.setOnShowListener {
            dialog?.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
                ?.apply {
                    val heightPixels = Resources.getSystem().displayMetrics.heightPixels
                    val height =
                        dialog?.findViewById<View>(com.google.android.material.R.id.touch_outside)?.height
                            ?: heightPixels
                    layoutParams.height = height
                }
        }
        gistFilterList.layoutManager = LinearLayoutManager(context)
        gistFilterList.adapter = adapter
        showData(items)
        searchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    viewModel.filterList(query ?: "", items)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }
            }
        )
    }

    override fun observeResults() {
        observe(viewModel.getFilterGistOwnerLiveData()) { state ->
            when (state) {
                is ViewState.Loading -> {
                }
                is ViewState.Success -> {
                    showData(state.data)
                }
                is ViewState.Failure -> {
                    showError()
                }
            }
        }
    }

    override fun showData(data: List<GistResponseModel>) {
        adapter.submitList(data)
        gistFilterList.visibility = View.VISIBLE
        gistFilterError.visibility = View.GONE
    }

    override fun showError() {
        gistFilterList.visibility = View.GONE
        gistFilterError.visibility = View.VISIBLE
    }
}
