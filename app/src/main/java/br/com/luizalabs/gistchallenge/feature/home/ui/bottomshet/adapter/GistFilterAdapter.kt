package br.com.luizalabs.gistchallenge.feature.home.ui.bottomshet.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.RecyclerView
import br.com.luizalabs.gistchallenge.R
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.gist_list_item.view.*

class GistFilterAdapter(
    private var items: MutableList<GistResponseModel> = mutableListOf(),
    private val lifecycleOwner: Lifecycle
) : RecyclerView.Adapter<GistFilterAdapter.GistFilterViewholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GistFilterViewholder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.gist_list_item, parent, false)
        return GistFilterViewholder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun submitList(data: List<GistResponseModel>) {
        this.items = data.toMutableList()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: GistFilterViewholder, position: Int) {
        holder.bind(items[position], lifecycleOwner)
    }

    class GistFilterViewholder(item: View) : RecyclerView.ViewHolder(item), LifecycleObserver {
        fun bind(
            item: GistResponseModel,
            lifecycleOwner: Lifecycle
        ) {
            lifecycleOwner.addObserver(this)
            itemView.gistListItemName.text =
                itemView.context.getString(R.string.gist_list_name, item.owner.name)
            itemView.gistListItemType.text =
                itemView.context.getString(R.string.gist_list_type, item.type)
            Picasso.get()
                .load(item.owner.avatar)
                .error(R.drawable.ic_placeholder)
                .into(itemView.gistListItemPhoto)

            itemView.gistListItemFavorite.visibility = View.INVISIBLE

            itemView.gistListItemContainer.isClickable = false
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun cancelRequest() {
            Picasso.get().cancelRequest(itemView.gistListItemFavorite)
        }
    }
}
