package br.com.luizalabs.gistchallenge.feature.home.ui.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import br.com.luizalabs.gistchallenge.common.domain.Error
import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.home.domain.GetPublicGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistsRequestModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GistDataSource(
    private val gistsUseCase: GetPublicGistsUseCaseImpl,
    private val scope: CoroutineScope
) :
    PageKeyedDataSource<Int, GistResponseModel>() {

    var state: MutableLiveData<ViewState<Boolean>> = MutableLiveData()
    @Volatile
    private var firstTime = true

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, GistResponseModel>
    ) {
        scope.launch {
            withContext(Dispatchers.Main) {
                updateState(ViewState.Loading(firstTime))
            }
            val result = withContext(Dispatchers.IO) {
                gistsUseCase.execute(Param(GistsRequestModel(params.requestedLoadSize, 1)))
            }
            withContext(Dispatchers.Main) {
                val list = when (result) {
                    is State.Success -> {
                        updateState(ViewState.Success(true))
                        result.data
                    }
                    is State.Failure -> {
                        firstTime = false
                        updateState(ViewState.Failure(result.error, firstTime))
                        listOf()
                    }
                }
                callback.onResult(list, null, 2)
                updateState(ViewState.Done)
            }
        }
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GistResponseModel>
    ) {
        scope.launch {
            withContext(Dispatchers.Main) {
                updateState(ViewState.Loading(false))
            }
            val result = withContext(Dispatchers.IO) {
                gistsUseCase.execute(Param(GistsRequestModel(params.requestedLoadSize, params.key)))
            }
            withContext(Dispatchers.Main) {
                val list = when (result) {
                    is State.Success -> {
                        result.data
                    }
                    is State.Failure -> {
                        listOf()
                    }
                }
                updateState(
                    if (list.isNotEmpty()) ViewState.Success(true) else ViewState.Failure(
                        Error.BusinessError(IllegalStateException()),
                        false
                    )
                )
                callback.onResult(list, params.key + 1)
                updateState(ViewState.Done)
            }
        }
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GistResponseModel>
    ) {
    }

    private fun updateState(state: ViewState<Boolean>) {
        this.state.value = state
    }
}
