package br.com.luizalabs.gistchallenge.feature.home.ui.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import br.com.luizalabs.gistchallenge.feature.home.domain.GetPublicGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import kotlinx.coroutines.CoroutineScope

class GistsDataSourceFactory(
    private val gistsUseCase: GetPublicGistsUseCaseImpl,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, GistResponseModel>() {

    val loadingLiveData = MutableLiveData<GistDataSource>()
    override fun create(): DataSource<Int, GistResponseModel> {
        val dataSource = GistDataSource(gistsUseCase, scope)
        loadingLiveData.postValue(dataSource)
        return dataSource
    }
}
