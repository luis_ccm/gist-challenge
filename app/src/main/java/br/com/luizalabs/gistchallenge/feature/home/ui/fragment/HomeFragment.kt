package br.com.luizalabs.gistchallenge.feature.home.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.luizalabs.gistchallenge.R
import br.com.luizalabs.gistchallenge.common.extension.observe
import br.com.luizalabs.gistchallenge.common.ui.ItemClickListener
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.ui.PublicGistsInteractor
import br.com.luizalabs.gistchallenge.feature.home.ui.bottomshet.GistsOwnerFilterBottomSheet
import br.com.luizalabs.gistchallenge.feature.home.ui.fragment.adapter.GistPagedAdapter
import br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment(), PublicGistsInteractor.View, Toolbar.OnMenuItemClickListener {

    private val viewModel by viewModel<HomeViewModel>()

    private val adapter: GistPagedAdapter by lazy {
        GistPagedAdapter(
            lifecycleOwner = lifecycle,
            itemClickListener = object : ItemClickListener<GistResponseModel> {
                override fun onItemClick(model: GistResponseModel) {
                    navigateToDetail(model)
                }
            },
            favoriteClickListener = object : ItemClickListener<GistResponseModel> {
                override fun onItemClick(model: GistResponseModel) {
                    insertFavorite(model)
                }
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeResults()
        initializeList()
        configureView()
    }

    override fun configureView() {
        homeToolbar.inflateMenu(R.menu.menu_search)
        homeToolbar.setOnMenuItemClickListener(this)
    }

    override fun showBottomSheet() {
        val bottomSheet = GistsOwnerFilterBottomSheet(adapter.currentList.orEmpty())
        bottomSheet.show(childFragmentManager, "")
    }

    override fun observeResults() {
        observe(viewModel.getStateLiveData()) { data ->
            when (data) {
                is ViewState.Loading -> {
                    if (data.fistTime)
                        showLoading()
                }
                is ViewState.Success -> showData()
                is ViewState.Failure -> {
                    if (data.fistTime)
                        showError()
                }
                is ViewState.Done -> {
                }
            }
        }

        observe(viewModel.gistsLiveData) {
            adapter.submitList(it)
        }

        observe(viewModel.getInsertLiveData()) { data ->
            Log.d(data.toString(), "observeResults: ")
        }
    }

    override fun initializeList() {
        gistList.adapter = adapter
    }

    override fun navigateToDetail(model: GistResponseModel) {
        findNavController().navigate(
            HomeFragmentDirections.actionNavigationHomeToTitleActivityGistUserDetail(
                model
            )
        )
    }

    override fun showError() {
        gistList.visibility = View.GONE
        gistLoading.visibility = View.GONE
        gistError.visibility = View.VISIBLE
    }

    override fun showLoading() {
        gistList.visibility = View.GONE
        gistLoading.visibility = View.VISIBLE
        gistError.visibility = View.GONE
    }

    override fun showData() {
        gistList.visibility = View.VISIBLE
        gistLoading.visibility = View.GONE
        gistError.visibility = View.GONE
    }

    override fun insertFavorite(model: GistResponseModel) {
        viewModel.insertFavorite(model)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        item?.let {
            if (it.itemId == R.id.app_bar_search) {
                showBottomSheet()
            }
        }
        return false
    }
}
