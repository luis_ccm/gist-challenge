package br.com.luizalabs.gistchallenge.feature.home.ui.fragment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import br.com.luizalabs.gistchallenge.R
import br.com.luizalabs.gistchallenge.common.ui.ItemClickListener
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.gist_list_item.view.*

class GistPagedAdapter(
    private val lifecycleOwner: Lifecycle,
    private val itemClickListener: ItemClickListener<GistResponseModel>,
    private val favoriteClickListener: ItemClickListener<GistResponseModel>
) :
    PagedListAdapter<GistResponseModel, GistPagedAdapter.GistViewHolder>(DiffUtilCallBack()) {

    class DiffUtilCallBack : DiffUtil.ItemCallback<GistResponseModel>() {
        override fun areItemsTheSame(
            oldItem: GistResponseModel,
            newItem: GistResponseModel
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: GistResponseModel,
            newItem: GistResponseModel
        ): Boolean {
            return oldItem.id == newItem.id &&
                oldItem.createdAt == newItem.createdAt &&
                oldItem.description == newItem.description &&
                oldItem.type == newItem.type &&
                oldItem.owner == newItem.owner
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GistViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.gist_list_item, parent, false)
        return GistViewHolder(view)
    }

    override fun onBindViewHolder(holder: GistViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        getItem(position)?.let { holder.bind(it, lifecycleOwner, itemClickListener, favoriteClickListener) }
    }

    class GistViewHolder(item: View) : RecyclerView.ViewHolder(item), LifecycleObserver {
        fun bind(
            response: GistResponseModel,
            lifecycleOwner: Lifecycle,
            itemClickListener: ItemClickListener<GistResponseModel>,
            favoriteClickListener: ItemClickListener<GistResponseModel>
        ) {
            lifecycleOwner.addObserver(this)
            itemView.gistListItemName.text =
                itemView.context.getString(R.string.gist_list_name, response.owner.name)
            itemView.gistListItemType.text =
                itemView.context.getString(R.string.gist_list_type, response.type)
            Picasso.get()
                .load(response.owner.avatar)
                .error(R.drawable.ic_placeholder)
                .into(itemView.gistListItemPhoto)

            itemView.gistListItemFavorite.setOnClickListener {
                itemView.gistListItemFavorite.playAnimation()
                favoriteClickListener.onItemClick(response)
            }
            itemView.gistListItemContainer.setOnClickListener {
                itemClickListener.onItemClick(
                    response
                )
            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun cancelRequest() {
            Picasso.get().cancelRequest(itemView.gistListItemPhoto)
        }
    }
}
