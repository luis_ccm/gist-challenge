package br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.extension.toViewState
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.home.domain.FilterGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistSearchModel
import br.com.luizalabs.gistchallenge.feature.home.ui.GistsOwnerFilterInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GistsOwnerFilterViewModel(private val filterGistsUseCase: FilterGistsUseCaseImpl) :
    ViewModel(), GistsOwnerFilterInteractor.ViewModel {

    private val filterGistOwnerLiveData = MutableLiveData<ViewState<List<GistResponseModel>>>()

    fun getFilterGistOwnerLiveData(): LiveData<ViewState<List<GistResponseModel>>> =
        filterGistOwnerLiveData

    override fun filterList(query: String, items: List<GistResponseModel>) {
        viewModelScope.launch {
            filterGistOwnerLiveData.value = ViewState.Loading()
            val result = withContext(Dispatchers.IO) {
                filterGistsUseCase.execute(Param(GistSearchModel(query, items)))
            }
            filterGistOwnerLiveData.value = result.toViewState()
            filterGistOwnerLiveData.value = ViewState.Done
        }
    }
}
