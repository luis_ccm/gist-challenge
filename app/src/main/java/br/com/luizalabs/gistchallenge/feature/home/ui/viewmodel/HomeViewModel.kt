package br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.extension.toViewState
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.home.domain.GetPublicGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.InsertGistFavoriteUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.ui.PublicGistsInteractor
import br.com.luizalabs.gistchallenge.feature.home.ui.datasource.GistDataSource
import br.com.luizalabs.gistchallenge.feature.home.ui.datasource.GistsDataSourceFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(
    private val getPublicGistsUseCase: GetPublicGistsUseCaseImpl,
    private val insertGistFavoriteUseCase: InsertGistFavoriteUseCaseImpl
) : ViewModel(), PublicGistsInteractor.ViewModel {

    val gistsLiveData: LiveData<PagedList<GistResponseModel>>
    private val insertLiveData = MutableLiveData<ViewState<Unit>>()
    private val dataSourceFactory: GistsDataSourceFactory =
        GistsDataSourceFactory(getPublicGistsUseCase, viewModelScope)
    private val pageSize = 50

    init {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .build()

        gistsLiveData =
            LivePagedListBuilder<Int, GistResponseModel>(dataSourceFactory, config).build()
    }

    fun getStateLiveData(): LiveData<ViewState<Boolean>> =
        Transformations.switchMap<GistDataSource, ViewState<Boolean>>(
            dataSourceFactory.loadingLiveData,
            GistDataSource::state
        )

    fun getInsertLiveData(): LiveData<ViewState<Unit>> = insertLiveData

    override fun insertFavorite(model: GistResponseModel) {
        viewModelScope.launch {
            insertLiveData.value = ViewState.Loading()
            val result = withContext(Dispatchers.IO) {
                insertGistFavoriteUseCase.execute(Param(model))
            }
            insertLiveData.value = result.toViewState()
            insertLiveData.value = ViewState.Done
        }
    }
}
