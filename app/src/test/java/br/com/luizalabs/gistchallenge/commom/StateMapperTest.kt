package br.com.luizalabs.gistchallenge.commom

import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.extension.toViewState
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import org.junit.Assert
import org.junit.Test

class StateMapperTest {

    @Test
    fun `GIVEN a State Success SHOULD toViewState return a compatible ViewState Success`() {
        val state = State.Success(true)
        val expectedViewState = ViewState.Success(true)

        val viewState = state.toViewState()

        Assert.assertEquals(expectedViewState, viewState)
    }

    @Test
    fun `GIVEN a State Failure SHOULD toViewState return a compatible ViewState Failure`() {
        val error = br.com.luizalabs.gistchallenge.common.domain.Error.BusinessError(Exception())
        val state: State<Error> = State.Failure(error)
        val expectedViewState = ViewState.Failure(error)

        val viewState = state.toViewState()

        Assert.assertEquals(expectedViewState, viewState)
    }
}
