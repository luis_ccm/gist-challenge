package br.com.luizalabs.gistchallenge.feature.favorite.data.mapper

import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model.GistFavoriteModel
import br.com.luizalabs.gistchallenge.feature.favorites.data.mapper.mapToDomain
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import org.junit.Assert
import org.junit.Test

class GistsFavoriteExtensionsTest {

    @Test
    fun `GIVEN an GistFavoriteModel mapToDomain method SHOULD return a valid GistFavorite`() {
        val expectedResult =
            GistFavorite(id = "100", name = "Luis Henrique", avatar = "https://www.google.com.br/")
        val data = GistFavoriteModel(
            id = "100",
            name = "Luis Henrique",
            avatar = "https://www.google.com.br/"
        )
        Assert.assertEquals(expectedResult, data.mapToDomain())
    }
}
