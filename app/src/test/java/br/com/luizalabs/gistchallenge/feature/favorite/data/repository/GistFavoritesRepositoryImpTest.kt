package br.com.luizalabs.gistchallenge.feature.favorite.data.repository

import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.local.GistFavoriteDao
import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model.GistFavoriteModel
import br.com.luizalabs.gistchallenge.feature.favorites.data.mapper.mapToDomain
import br.com.luizalabs.gistchallenge.feature.favorites.data.repository.GistsFavoriteRepositoryImpl
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GistFavoritesRepositoryImpTest {

    private lateinit var repository: GistsFavoritesRepository

    @MockK
    private lateinit var localDao: GistFavoriteDao

    private val data: GistFavoriteModel = GistFavoriteModel(id = "", avatar = "", name = "")

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repository = GistsFavoriteRepositoryImpl(localDao)
    }

    @Test
    fun `GIVEN a valid CALL getFavorites method SHOULD getFavoritesGists on GistFavoriteDao be invoked once`() {
        runBlockingTest {
            coEvery { localDao.getFavoritesGists() } returns listOf(data)

            repository.getFavorites()

            coVerify(exactly = 1) { localDao.getFavoritesGists() }
        }
    }

    @Test
    fun `GIVEN a valid CALL getFavorites method SHOULD return a valid Response`() {
        runBlockingTest {
            coEvery { localDao.getFavoritesGists() } returns listOf(data)

            val result = repository.getFavorites()

            Assert.assertEquals(listOf(data.mapToDomain()), result)
        }
    }

    @Test(expected = Exception::class)
    fun `GIVEN an invalid CALL getFavorites method SHOULD throws a Exception`() {
        runBlockingTest {
            coEvery { localDao.getFavoritesGists() } throws Exception()
            repository.getFavorites()
        }
    }

    @Test
    fun `GIVEN a valid CALL insertFavorite method SHOULD insert on GistFavoriteDao be invoked once`() {
        runBlockingTest {
            coEvery { localDao.insert(any()) } returns Unit

            repository.insertFavorite(GistResponseModel.empty())

            coVerify(exactly = 1) { localDao.insert(data) }
        }
    }

    @Test
    fun `GIVEN a valid CALL insertFavorite method SHOULD return a valid Response`() {
        runBlockingTest {
            coEvery { localDao.insert(any()) } returns Unit

            val result = repository.insertFavorite(GistResponseModel.empty())

            Assert.assertEquals(Unit, result)
        }
    }

    @Test(expected = Exception::class)
    fun `GIVEN an invalid CALL insertFavorite method SHOULD throws a Exception`() {
        runBlockingTest {
            coEvery { localDao.insert(data) } throws Exception()
            repository.getFavorites()
        }
    }

    @Test
    fun `GIVEN a valid CALL removeFavorite method SHOULD deleteFavorite on GistFavoriteDao be invoked once`() {
        runBlockingTest {
            coEvery { localDao.deleteFavorite(any()) } returns Unit

            repository.removeFavorite("")

            coVerify(exactly = 1) { localDao.deleteFavorite("") }
        }
    }

    @Test
    fun `GIVEN a valid CALL removeFavorite method SHOULD deleteFavorite return a valid Response`() {
        runBlockingTest {
            coEvery { localDao.deleteFavorite(any()) } returns Unit

            val result = repository.removeFavorite("")

            Assert.assertEquals(Unit, result)
        }
    }

    @Test(expected = Exception::class)
    fun `GIVEN an invalid CALL removeFavorite method SHOULD throws a Exception`() {
        runBlockingTest {
            coEvery { localDao.deleteFavorite(any()) } throws Exception()
            repository.getFavorites()
        }
    }
}
