package br.com.luizalabs.gistchallenge.feature.favorite.domain

import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.feature.favorites.domain.GetFavoritesUseCaseImp
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetFavoritesUseCaseTest {

    private lateinit var useCaseImpl: GetFavoritesUseCaseImp

    @MockK
    private lateinit var repository: GistsFavoritesRepository

    private val data = GistFavorite(id = "100", name = "Luis", avatar = "")

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        useCaseImpl = GetFavoritesUseCaseImp(repository)
    }

    @Test
    fun `GIVEN a valid CALL getFavoritesUseCase SHOULD return a Success result`() {
        runBlockingTest {
            coEvery { repository.getFavorites() } returns listOf(data)

            val result = useCaseImpl.execute()

            val expectedResult = State.Success(listOf(data))
            Assert.assertEquals(expectedResult, result)
        }
    }

    @Test
    fun `GIVEN an invalid CALL getFavoritesUseCase SHOULD return a Failure result`() {
        runBlockingTest {
            val exception = Exception()
            coEvery { repository.getFavorites() } throws exception

            val result = useCaseImpl.execute()

            assert(result is State.Failure)
        }
    }
}
