package br.com.luizalabs.gistchallenge.feature.favorite.domain

import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.feature.favorites.domain.RemoveFavoriteUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class RemoveFavoriteUseCaseTest {
    private lateinit var useCaseImpl: RemoveFavoriteUseCaseImpl

    @MockK
    private lateinit var repository: GistsFavoritesRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        useCaseImpl = RemoveFavoriteUseCaseImpl(repository)
    }

    @Test
    fun `GIVEN a valid CALL removeFavoriteCase SHOULD remove on GistsFavoritesRepository be invoked once`() {
        runBlockingTest {
            coEvery { repository.removeFavorite("") } returns Unit

            useCaseImpl.execute(Param(""))

            coVerify(exactly = 1) { repository.removeFavorite("") }
        }
    }

    @Test
    fun `GIVEN a valid CALL removeFavoriteCase SHOULD return a Success result`() {
        runBlockingTest {
            coEvery { repository.removeFavorite(any()) } returns Unit

            val result = useCaseImpl.execute(Param(""))

            coVerify(exactly = 1) { repository.removeFavorite("") }

            val expectedResult = State.Success(Unit)
            Assert.assertEquals(expectedResult, result)
        }
    }

    @Test
    fun `GIVEN an invalid CALL removeFavoriteCase SHOULD return a Failure result`() {
        runBlockingTest {
            val exception = Exception()
            coEvery { repository.removeFavorite(any()) } throws exception

            val result = useCaseImpl.execute(Param(""))
            assert(result is State.Failure)
        }
    }

    @Test
    fun `GIVEN a valid CALL insertGistFavoriteUseCase with empty parameters SHOULD method SHOULD return a Failure result`() {
        runBlockingTest {
            val exception = Exception()
            coEvery { repository.removeFavorite(any()) } throws exception

            val result = useCaseImpl.execute()
            assert(result is State.Failure)
        }
    }
}
