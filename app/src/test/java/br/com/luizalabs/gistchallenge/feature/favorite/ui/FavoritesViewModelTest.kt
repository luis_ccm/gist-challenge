package br.com.luizalabs.gistchallenge.feature.favorite.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.luizalabs.gistchallenge.common.domain.Error
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.extension.toViewState
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.favorites.domain.GetFavoritesUseCaseImp
import br.com.luizalabs.gistchallenge.feature.favorites.domain.RemoveFavoriteUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.favorites.domain.model.GistFavorite
import br.com.luizalabs.gistchallenge.feature.favorites.ui.viewModel.GistFavoriteViewModel
import br.com.luizalabs.gistchallenge.util.rule.CoroutineTestRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class FavoritesViewModelTest {
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val coroutineTestRule: CoroutineTestRule = CoroutineTestRule()

    private lateinit var viewModel: GistFavoriteViewModel

    @MockK
    private lateinit var useCase: GetFavoritesUseCaseImp

    @MockK
    private lateinit var removeUseCase: RemoveFavoriteUseCaseImpl

    @MockK(relaxed = true)
    private lateinit var observer: Observer<ViewState<List<GistFavorite>>>

    @MockK(relaxed = true)
    private lateinit var observerRemove: Observer<ViewState<Unit>>

    private val data = GistFavorite(id = "100", name = "Luis", avatar = "teste")

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        viewModel = GistFavoriteViewModel(useCase, removeUseCase)
    }

    @Test
    fun `GIVEN a VALID Data favoritesLiveData emits a Loading, Success and Done ViewStates`() =
        runBlockingTest {
            val state = State.Success(listOf(data))

            coEvery { useCase.execute() } returns state

            viewModel.getFavoritesLiveData().observeForever(observer)
            viewModel.getFavorites()

            verify { observer.onChanged(ViewState.Loading(true)) }
            verify { observer.onChanged(state.toViewState()) }
            verify { observer.onChanged(ViewState.Done) }
        }

    @Test
    fun `GIVEN a INVALID Data favoritesLiveData emits a Loading, Failure and Done ViewStates`() =
        runBlockingTest {
            val state = State.Failure(Error.BusinessError(Exception()))

            coEvery { useCase.execute() } returns state

            viewModel.getFavoritesLiveData().observeForever(observer)
            viewModel.getFavorites()

            verify { observer.onChanged(ViewState.Loading(true)) }
            verify { observer.onChanged(state.toViewState()) }
            verify { observer.onChanged(ViewState.Done) }
        }

    @Test
    fun `GIVEN a VALID Data removeLiveData emits a Loading, Success and Done ViewStates`() =
        runBlockingTest {
            val state = State.Success(Unit)

            coEvery { removeUseCase.execute(any()) } returns state

            viewModel.getRemoveLiveData().observeForever(observerRemove)
            viewModel.removeFavorite(data)

            verify { observerRemove.onChanged(ViewState.Loading(true)) }
            verify { observerRemove.onChanged(state.toViewState()) }
            verify { observerRemove.onChanged(ViewState.Done) }
        }

    @Test
    fun `GIVEN a INVALID Data removeLiveData emits a Loading, Failure and Done ViewStates`() =
        runBlockingTest {
            val state = State.Success(Unit)

            coEvery { removeUseCase.execute(any()) } returns state

            viewModel.getRemoveLiveData().observeForever(observerRemove)
            viewModel.removeFavorite(data)

            verify { observerRemove.onChanged(ViewState.Loading(true)) }
            verify { observerRemove.onChanged(state.toViewState()) }
            verify { observerRemove.onChanged(ViewState.Done) }
        }
}
