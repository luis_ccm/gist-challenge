package br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.impl

import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.GistsRemote
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistModelPayload
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistOwnerPayload
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.service.GistsService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.Calendar

@ExperimentalCoroutinesApi
class GistNetworkImplTest {

    private lateinit var network: GistsRemote

    @MockK
    private lateinit var service: GistsService

    private lateinit var data: GistModelPayload

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        network = GistsRemoteImpl(service)
        val createdAt = "2020-10-20T09:32:37Z"
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val calendar = Calendar.getInstance()
        calendar.time = format.parse(createdAt)!!
        val time = calendar.time
        data = GistModelPayload(
            id = "f242b8f0dcffadb4aa01a0b7e53004d9",
            description = "",
            files = emptyMap(),
            createdAt = time,
            owner = GistOwnerPayload(
                id = 25447658,
                url = "https:/github.com/GrahamcOfBorg",
                login = "GrahamcOfBorg",
                avatarUrl = "https://avatars2.githubusercontent.com/u/25447658?v=4"
            )
        )
    }

    @Test
    fun `GIVEN a valid CALL getPublicGists method SHOULD getPublicGists on GistsService be invoked once`() {
        runBlockingTest {
            coEvery { service.getPublicGists(50, 1) } returns listOf(data)

            network.getPublicGists(50, 1)

            coVerify(exactly = 1) { service.getPublicGists(50, 1) }
        }
    }

    @Test
    fun `GIVEN a valid CALL getPublicGists method SHOULD return a valid Response`() {
        runBlockingTest {
            coEvery { service.getPublicGists(50, 1) } returns listOf(data)

            val result = network.getPublicGists(50, 1)

            Assert.assertEquals(listOf(data), result)
        }
    }

    @Test(expected = Exception::class)
    fun `GIVEN an invalid CALL getPublicGists method SHOULD throws a Exception`() {
        runBlockingTest {
            coEvery { service.getPublicGists(50, 1) } throws Exception()
            network.getPublicGists(50, 1)
        }
    }
}
