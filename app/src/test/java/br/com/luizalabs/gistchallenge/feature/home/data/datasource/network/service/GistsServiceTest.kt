package br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.service

import android.app.Application
import android.content.Context
import br.com.luizalabs.gistchallenge.di.FeatureModules
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistModelPayload
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistOwnerPayload
import br.com.luizalabs.gistchallenge.util.TestUtil
import com.google.gson.GsonBuilder
import io.mockk.MockKAnnotations
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.concurrent.TimeUnit

class GistsServiceTest : KoinTest {

    private val context: Context = mockk<Application>()
    private val mockWebServer: MockWebServer = MockWebServer()
    private lateinit var gistsService: GistsService
    private var success = true

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        startKoin {
            androidContext(context)
            modules(FeatureModules.modules)
        }

        gistsService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(GistsService::class.java)

        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when (success) {
                    true -> {
                        val mockResponse = MockResponse()
                        mockResponse.setResponseCode(HttpURLConnection.HTTP_OK)
                        mockResponse.setBody(TestUtil.jsonText)
                        mockResponse.addHeader("content-type", "application/json")
                        mockResponse.setBodyDelay(2, TimeUnit.SECONDS)
                        mockResponse
                    }
                    else -> {
                        val mockResponse = MockResponse()
                        mockResponse.setResponseCode(412)
                        mockResponse.setBodyDelay(2, TimeUnit.SECONDS)
                        mockResponse
                    }
                }
            }
        }
    }

    @Test
    fun `GIVEN a valid CALL getPublicGists method SHOULD return a valid Response`() {
        runBlocking {
            success = true
            val createdAt = "2020-10-20T09:32:37Z"
            val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val calendar = Calendar.getInstance()
            calendar.time = format.parse(createdAt)!!
            val time = calendar.time
            val data = GistModelPayload(
                id = "f242b8f0dcffadb4aa01a0b7e53004d9",
                description = "",
                files = emptyMap(),
                createdAt = time,
                owner = GistOwnerPayload(
                    id = 25447658,
                    url = "https:/github.com/GrahamcOfBorg",
                    login = "GrahamcOfBorg",
                    avatarUrl = "https://avatars2.githubusercontent.com/u/25447658?v=4"
                )
            )

            val payload = gistsService.getPublicGists(50, 1)

            Assert.assertEquals(data, payload.first())
        }
    }

    @Test(expected = HttpException::class)
    fun `GIVEN an invalid CALL getPublicGists method SHOULD throws a HttpException`() {
        runBlocking {
            success = false
            gistsService.getPublicGists(50, 1)
        }
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
        stopKoin()
    }
}
