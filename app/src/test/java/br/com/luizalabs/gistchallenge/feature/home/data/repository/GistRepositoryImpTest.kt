package br.com.luizalabs.gistchallenge.feature.home.data.repository

import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.GistsRemote
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistModelPayload
import br.com.luizalabs.gistchallenge.feature.home.data.datasource.network.model.GistOwnerPayload
import br.com.luizalabs.gistchallenge.feature.home.data.mapper.mapToDomain
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistsRequestModel
import br.com.luizalabs.gistchallenge.feature.home.domain.repository.GistsRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.Calendar

@ExperimentalCoroutinesApi
class GistRepositoryImpTest {

    private lateinit var repository: GistsRepository

    @MockK
    private lateinit var network: GistsRemote

    private lateinit var data: GistModelPayload

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repository = GistsRepositoryImpl(network)
        val createdAt = "2020-10-20T09:32:37Z"
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val calendar = Calendar.getInstance()
        calendar.time = format.parse(createdAt)!!
        val time = calendar.time
        val typeMap = mapOf<String, Any>(pair = Pair("type", "application/x-ruby"))
        data = GistModelPayload(
            id = "f242b8f0dcffadb4aa01a0b7e53004d9",
            description = "",
            files = typeMap,
            createdAt = time,
            owner = GistOwnerPayload(
                id = 25447658,
                url = "https:/github.com/GrahamcOfBorg",
                login = "GrahamcOfBorg",
                avatarUrl = "https://avatars2.githubusercontent.com/u/25447658?v=4"
            )
        )
    }

    @Test
    fun `GIVEN a valid CALL getPublicGists method SHOULD getPublicGists on GistsRemote be invoked once`() {
        runBlockingTest {
            coEvery { network.getPublicGists(50, 1) } returns listOf(data)

            network.getPublicGists(50, 1)

            coVerify(exactly = 1) { repository.getPublicGists(GistsRequestModel(50, 1)) }
        }
    }

    @Test
    fun `GIVEN a valid CALL getPublicGists method SHOULD return a valid Response`() {
        runBlockingTest {
            coEvery { network.getPublicGists(50, 1) } returns listOf(data)

            val result = repository.getPublicGists(GistsRequestModel(50, 1))

            Assert.assertEquals(listOf(data.mapToDomain()), result)
        }
    }

    @Test(expected = Exception::class)
    fun `GIVEN an invalid CALL getPublicGists method SHOULD throws a Exception`() {
        runBlockingTest {
            coEvery { network.getPublicGists(50, 1) } throws Exception()
            network.getPublicGists(50, 1)
        }
    }
}
