package br.com.luizalabs.gistchallenge.feature.home.domain

import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistOwnerResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistSearchModel
import io.mockk.MockKAnnotations
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class FilterGistsUseCaseTest {

    private lateinit var useCaseImpl: FilterGistsUseCaseImpl

    private val owner = GistOwnerResponseModel(
        id = 1,
        name = "Luis Henrique",
        url = "https://pt.stackoverflow.com/",
        avatar = "https://www.google.com.br/"
    )

    private val data = GistResponseModel(
        id = "100",
        description = "Teste Unitário",
        owner = owner,
        type = "application/x-ruby",
        createdAt = "10/10/2020"
    )

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        useCaseImpl = FilterGistsUseCaseImpl()
    }

    @Test
    fun `GIVEN a valid CALL filterGistsUseCase SHOULD return a Success result`() {
        runBlockingTest {

            val result = useCaseImpl.execute(Param(GistSearchModel("", listOf(data))))

            val expectedResult = State.Success(listOf(data))
            Assert.assertEquals(expectedResult, result)
        }
    }

    @Test
    fun `GIVEN an invalid CALL filterGistsUseCase SHOULD method SHOULD return a Failure result`() {
        runBlockingTest {
            val result = useCaseImpl.execute(Param(GistSearchModel("xzy", listOf(data))))

            assert(result is State.Failure)
        }
    }

    @Test
    fun `GIVEN a valid CALL filterGistsUseCasewith empty parameters SHOULD method SHOULD return a Failure result`() {
        runBlockingTest {
            val result = useCaseImpl.execute()

            assert(result is State.Failure)
        }
    }
}
