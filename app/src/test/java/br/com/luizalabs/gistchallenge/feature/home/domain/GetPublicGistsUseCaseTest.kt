package br.com.luizalabs.gistchallenge.feature.home.domain

import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistOwnerResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistsRequestModel
import br.com.luizalabs.gistchallenge.feature.home.domain.repository.GistsRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetPublicGistsUseCaseTest {

    private lateinit var useCaseImpl: GetPublicGistsUseCaseImpl

    @MockK
    private lateinit var repository: GistsRepository

    private val owner = GistOwnerResponseModel(
        id = 1,
        name = "Luis Henrique",
        url = "https://pt.stackoverflow.com/",
        avatar = "https://www.google.com.br/"
    )

    private val data = GistResponseModel(
        id = "100",
        description = "Teste Unitário",
        owner = owner,
        type = "application/x-ruby",
        createdAt = "10/10/2020"
    )

    private val requestModel = GistsRequestModel(50, 1)

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        useCaseImpl = GetPublicGistsUseCaseImpl(repository)
    }

    @Test
    fun `GIVEN a valid CALL getPublicGistsUseCase SHOULD getPublicGists on GistsRepository be invoked once`() {
        runBlockingTest {
            coEvery { repository.getPublicGists(any()) } returns listOf(data)

            useCaseImpl.execute(Param(requestModel))

            coVerify(exactly = 1) { repository.getPublicGists(GistsRequestModel(50, 1)) }
        }
    }

    @Test
    fun `GIVEN a valid CALL getPublicGistsUseCase SHOULD return a Success result`() {
        runBlockingTest {
            coEvery { repository.getPublicGists(any()) } returns listOf(data)

            val result = useCaseImpl.execute(Param(requestModel))

            val expectedResult = State.Success(listOf(data))
            Assert.assertEquals(expectedResult, result)
        }
    }

    @Test
    fun `GIVEN an invalid CALL getPublicGistsUseCase SHOULD return a Failure result`() {
        runBlockingTest {
            val exception = Exception()
            coEvery { repository.getPublicGists(any()) } throws exception

            val result = useCaseImpl.execute(Param(requestModel))

            assert(result is State.Failure)
        }
    }

    @Test
    fun `GIVEN a valid CALL getPublicGistsUseCase with empty parameters SHOULD return a Failure result`() {
        runBlockingTest {
            val exception = Exception()
            coEvery { repository.getPublicGists(any()) } throws exception

            val result = useCaseImpl.execute()

            assert(result is State.Failure)
        }
    }
}
