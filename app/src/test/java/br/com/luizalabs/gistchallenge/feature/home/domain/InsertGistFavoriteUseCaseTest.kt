package br.com.luizalabs.gistchallenge.feature.home.domain

import br.com.luizalabs.gistchallenge.common.domain.Param
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.feature.favorites.domain.repository.GistsFavoritesRepository
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistOwnerResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class InsertGistFavoriteUseCaseTest {
    private lateinit var useCaseImpl: InsertGistFavoriteUseCaseImpl

    @MockK
    private lateinit var repository: GistsFavoritesRepository

    private val owner = GistOwnerResponseModel(
        id = 1,
        name = "Luis Henrique",
        url = "https://pt.stackoverflow.com/",
        avatar = "https://www.google.com.br/"
    )

    private val data = GistResponseModel(
        id = "100",
        description = "Teste Unitário",
        owner = owner,
        type = "application/x-ruby",
        createdAt = "10/10/2020"
    )

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        useCaseImpl = InsertGistFavoriteUseCaseImpl(repository)
    }

    @Test
    fun `GIVEN a valid CALL insertGistFavoriteUseCase SHOULD insert on GistsFavoritesRepository be invoked once`() {
        runBlockingTest {
            coEvery { repository.insertFavorite(any()) } returns Unit

            useCaseImpl.execute(Param(data))

            coVerify(exactly = 1) { repository.insertFavorite(data) }
        }
    }

    @Test
    fun `GIVEN a valid CALL insertGistFavoriteUseCase SHOULD return a Success result`() {
        runBlockingTest {
            coEvery { repository.insertFavorite(any()) } returns Unit

            val result = useCaseImpl.execute(Param(data))

            coVerify(exactly = 1) { repository.insertFavorite(data) }

            val expectedResult = State.Success(Unit)
            Assert.assertEquals(expectedResult, result)
        }
    }

    @Test
    fun `GIVEN an invalid CALL insertGistFavoriteUseCase SHOULD return a Failure result`() {
        runBlockingTest {
            val exception = Exception()
            coEvery { repository.insertFavorite(any()) } throws exception

            val result = useCaseImpl.execute(Param(data))
            assert(result is State.Failure)
        }
    }

    @Test
    fun `GIVEN a valid CALL insertGistFavoriteUseCase with empty parameters SHOULD return a Failure result`() {
        runBlockingTest {
            val exception = Exception()
            coEvery { repository.insertFavorite(any()) } throws exception

            val result = useCaseImpl.execute()
            assert(result is State.Failure)
        }
    }
}
