package br.com.luizalabs.gistchallenge.feature.home.domain.mapper

import br.com.luizalabs.gistchallenge.feature.favorites.data.datasource.model.GistFavoriteModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistOwnerResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import org.junit.Assert
import org.junit.Test

class GistsResponseExtensionsTest {

    @Test
    fun `GIVEN an GistResponseModel mapToData method SHOULD return a valid GistFavoriteModel`() {
        val expectedResult = GistFavoriteModel(
            id = "100",
            name = "Luis Henrique",
            avatar = "https://www.google.com.br/"
        )
        val owner = GistOwnerResponseModel(
            id = 1,
            name = "Luis Henrique",
            url = "https://pt.stackoverflow.com/",
            avatar = "https://www.google.com.br/"
        )

        val data = GistResponseModel(
            id = "100",
            description = "Teste Unitário",
            owner = owner,
            type = "application/x-ruby",
            createdAt = "10/10/2020"
        )
        Assert.assertEquals(expectedResult, data.mapToData())
    }
}
