package br.com.luizalabs.gistchallenge.feature.home.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.luizalabs.gistchallenge.common.domain.Error
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.extension.toViewState
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.home.domain.FilterGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistOwnerResponseModel
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel.GistsOwnerFilterViewModel
import br.com.luizalabs.gistchallenge.util.rule.CoroutineTestRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class GistsOwnerFilterViewModelTest {
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val coroutineTestRule: CoroutineTestRule = CoroutineTestRule()

    private lateinit var viewModel: GistsOwnerFilterViewModel

    @MockK
    private lateinit var useCase: FilterGistsUseCaseImpl

    @MockK(relaxed = true)
    private lateinit var observer: Observer<ViewState<List<GistResponseModel>>>

    private val owner = GistOwnerResponseModel(
        id = 1,
        name = "Luis Henrique",
        url = "https://pt.stackoverflow.com/",
        avatar = "https://www.google.com.br/"
    )

    private val data = GistResponseModel(
        id = "100",
        description = "Teste Unitário",
        owner = owner,
        type = "application/x-ruby",
        createdAt = "10/10/2020"
    )

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        viewModel = GistsOwnerFilterViewModel(useCase)
    }

    @Test
    fun `GIVEN a VALID Data filterGistOwnerLiveData emits a Loading, Success and Done ViewStates`() =
        runBlockingTest {
            val state = State.Success(listOf(data))

            coEvery { useCase.execute(any()) } returns state

            viewModel.getFilterGistOwnerLiveData().observeForever(observer)
            viewModel.filterList("", emptyList())

            verify { observer.onChanged(ViewState.Loading(true)) }
            verify { observer.onChanged(state.toViewState()) }
            verify { observer.onChanged(ViewState.Done) }
        }

    @Test
    fun `GIVEN a INVALID Data filterGistOwnerLiveData emits a Loading, Failure and Done ViewStates`() =
        runBlockingTest {
            val state = State.Failure(Error.BusinessError(Exception()))

            coEvery { useCase.execute(any()) } returns state

            viewModel.getFilterGistOwnerLiveData().observeForever(observer)
            viewModel.filterList("", emptyList())

            verify { observer.onChanged(ViewState.Loading(true)) }
            verify { observer.onChanged(state.toViewState()) }
            verify { observer.onChanged(ViewState.Done) }
        }
}
