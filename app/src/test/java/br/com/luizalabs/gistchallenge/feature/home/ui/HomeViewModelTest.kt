package br.com.luizalabs.gistchallenge.feature.home.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.luizalabs.gistchallenge.common.domain.Error
import br.com.luizalabs.gistchallenge.common.domain.State
import br.com.luizalabs.gistchallenge.common.extension.toViewState
import br.com.luizalabs.gistchallenge.common.ui.ViewState
import br.com.luizalabs.gistchallenge.feature.home.domain.GetPublicGistsUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.InsertGistFavoriteUseCaseImpl
import br.com.luizalabs.gistchallenge.feature.home.domain.model.GistResponseModel
import br.com.luizalabs.gistchallenge.feature.home.ui.viewmodel.HomeViewModel
import br.com.luizalabs.gistchallenge.util.rule.CoroutineTestRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class HomeViewModelTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val coroutineTestRule: CoroutineTestRule = CoroutineTestRule()

    private lateinit var viewModel: HomeViewModel

    @MockK
    private lateinit var publicGistsUseCase: GetPublicGistsUseCaseImpl

    @MockK
    private lateinit var insertGistFavoriteUseCase: InsertGistFavoriteUseCaseImpl

    @MockK(relaxed = true)
    private lateinit var observer: Observer<ViewState<Unit>>

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        viewModel = HomeViewModel(publicGistsUseCase, insertGistFavoriteUseCase)
    }

    @Test
    fun `GIVEN a VALID Data InsertLiveData emits a Loading, Success and Done ViewStates`() =
        runBlockingTest {
            val state = State.Success(Unit)

            coEvery { insertGistFavoriteUseCase.execute(any()) } returns state

            viewModel.getInsertLiveData().observeForever(observer)
            viewModel.insertFavorite(GistResponseModel.empty())

            verify { observer.onChanged(ViewState.Loading(true)) }
            verify { observer.onChanged(state.toViewState()) }
            verify { observer.onChanged(ViewState.Done) }
        }

    @Test
    fun `GIVEN a INVALID Data InsertLiveData emits a Loading, Failure and Done ViewStates`() =
        runBlockingTest {
            val state = State.Failure(Error.BusinessError(Exception()))

            coEvery { insertGistFavoriteUseCase.execute(any()) } returns state

            viewModel.getInsertLiveData().observeForever(observer)
            viewModel.insertFavorite(GistResponseModel.empty())

            verify { observer.onChanged(ViewState.Loading(true)) }
            verify { observer.onChanged(state.toViewState()) }
            verify { observer.onChanged(ViewState.Done) }
        }
}
