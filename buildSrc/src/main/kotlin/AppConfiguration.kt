object AppConfiguration {

    object Plugins {
        const val androidApplication = "com.android.application"
        const val kotlinAndroid = "android"
        const val kotlinAndroidExtensions = "android.extensions"
        const val kapt = "kapt"
        const val kotlinKapt = "kotlin-kapt"
        const val kotlinAndroidPlugin = "kotlin-android"
        const val safeArgs = "androidx.navigation.safeargs.kotlin"
    }

    const val compileSdkVersion = 30
    const val applicationId = "br.com.luizalabs.gistchallenge"
    const val minSdkVersion = 21
    const val targetSdkVersion = 30
    const val versionCode = 1
    const val versionName = "1.0"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

    sealed class BuildType(
        val name: String,
        val isMinifyEnabled: Boolean = false,
        val defaultProguardFile: String = "",
        val defaultProguardRules: String = "",
        val suffix: String = ""
    ) {
        object Release : BuildType(
            name = "release",
            isMinifyEnabled = true,
            defaultProguardFile = "proguard-android-optimize.txt",
            defaultProguardRules = "proguard-rules.pro"
        )

        object Debug : BuildType(
            name = "debug",
            suffix = ".debug"
        )
    }

    object Task {
        const val preBuild = "preBuild"
        const val ktlintCheck = "lintCheck"
    }

    sealed class Dimension(val name: String) {
        object Type : Dimension("type")
    }

    sealed class ProductFlavor(val name: String, val dimension: Dimension) {
        object Netshoes : ProductFlavor("Netshoes", Dimension.Type)
        object MagazineLuiza : ProductFlavor("MagazineLuiza", Dimension.Type)
    }

    val libsImplementations = listOf(
        Libs.kotlin_stdlib,
        Libs.core_ktx,
        Libs.appcompat,
        Libs.material,
        Libs.constraintlayout,
        Libs.navigation_fragment_ktx,
        Libs.navigation_ui_ktx,
        Libs.circleimageview,
        Libs.lottie,
        Libs.koin_core,
        Libs.koin_androidx_viewmodel,
        Libs.koin_android,
        Libs.retrofit,
        Libs.converter_gson,
        Libs.kotlinx_coroutines_android,
        Libs.kotlinx_coroutines_core,
        Libs.gson,
        Libs.paging_runtime,
        Libs.okhttp,
        Libs.picasso,
        Libs.room_compiler,
        Libs.room_runtime,
        Libs.room_ktx
    )

    val testImplementationLibs = listOf(
        Libs.junit_junit,
        Libs.koin_test,
        Libs.kotlinx_coroutines_test,
        Libs.mockito_core,
        Libs.core_testing,
        Libs.mockito_core,
        Libs.espresso_core,
        Libs.mockito_kotlin,
        Libs.mockk,
        Libs.mockwebserver
    )

    val androidTestImplementationLibs = listOf(
        Libs.androidx_test_ext_junit,
        Libs.espresso_core
    )

    object Lint {
        sealed class Task(
            val description: String = "",
            val main: String = "",
            val args: List<String> = emptyList(),
            val group: String = "format"
        ) {
            object Check : Task(
                description = "Check Kotlin code style.",
                main = "com.pinterest.ktlint.Main",
                args = listOf("src/**/*.kt")
            )

            object Format : Task(
                description = "Fix Kotlin code style deviations.",
                main = "com.pinterest.ktlint.Main",
                args = listOf("-F", "src/**/*.kt")
            )
        }

    }
}