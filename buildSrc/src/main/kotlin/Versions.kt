import kotlin.String
import org.gradle.plugin.use.PluginDependenciesSpec
import org.gradle.plugin.use.PluginDependencySpec

/**
 * Generated by https://github.com/jmfayard/buildSrcVersions
 *
 * Find which updates are available by running
 *     `$ ./gradlew buildSrcVersions`
 * This will only update the comments.
 *
 * YOU are responsible for updating manually the dependency version.
 */
object Versions {
    const val de_fayard_buildsrcversions_gradle_plugin: String = "0.6.1" // available: "0.7.0"

    const val navigation_safe_args_gradle_plugin: String = "2.3.0" // available: "2.3.1"

    const val com_android_tools_build_gradle: String = "4.0.1" // available: "4.1.0"

    const val androidx_test_ext_junit: String = "1.1.2"

    const val navigation_fragment_ktx: String = "2.3.1"

    const val com_squareup_retrofit2: String = "2.9.0"

    const val org_jetbrains_kotlinx: String = "1.3.3" // available: "1.3.9"

    const val com_squareup_okhttp3: String = "4.9.0"

    const val org_jetbrains_kotlin: String = "1.3.72" // available: "1.4.10"

    const val navigation_ui_ktx: String = "2.3.1"

    const val constraintlayout: String = "2.0.2"

    const val circleimageview: String = "3.1.0"

    const val mockito_kotlin: String = "2.1.0"

    const val paging_runtime: String = "2.1.2"

    const val androidx_room: String = "2.2.5"

    const val espresso_core: String = "3.3.0"

    const val core_testing: String = "2.1.0"

    const val mockito_core: String = "3.5.13" // available: "3.5.15"

    const val junit_junit: String = "4.12" // available: "4.13.1"

    const val lint_gradle: String = "27.0.1" // available: "27.1.0"

    const val appcompat: String = "1.2.0"

    const val core_ktx: String = "1.3.2"

    const val material: String = "1.2.1"

    const val org_koin: String = "2.1.6"

    const val picasso: String = "2.71828"

    const val ktlint: String = "0.38.1" // available: "0.39.0"

    const val lottie: String = "3.4.4"

    const val aapt2: String = "4.0.1-6197926" // available: "4.1.0-6503028"

    const val mockk: String = "1.10.2"

    const val gson: String = "2.8.6"

    /**
     * Current version: "6.1.1"
     * See issue 19: How to update Gradle itself?
     * https://github.com/jmfayard/buildSrcVersions/issues/19
     */
    const val gradleLatestVersion: String = "6.7"
}

/**
 * See issue #47: how to update buildSrcVersions itself
 * https://github.com/jmfayard/buildSrcVersions/issues/47
 */
val PluginDependenciesSpec.buildSrcVersions: PluginDependencySpec
    inline get() =
            id("de.fayard.buildSrcVersions").version(Versions.de_fayard_buildsrcversions_gradle_plugin)
